# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown, doctest_checker


class StockShipmentOutDoAndPrintTestCase(ModuleTestCase):
    """Test Stock Shipment Out Do And Print module"""
    module = 'stock_shipment_out_go_and_print'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            StockShipmentOutDoAndPrintTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_stock_shipment_out.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
