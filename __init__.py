# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .shipment import (
    ShipmentOut, ShipmentOutDoAndPrint,
    Configuration, ConfigurationShipmentPrintState)


def register():
    Pool.register(
        Configuration,
        ShipmentOut,
        ConfigurationShipmentPrintState,
        module='stock_shipment_out_go_and_print', type_='model')
    Pool.register(
        ShipmentOutDoAndPrint,
        module='stock_shipment_out_go_and_print', type_='wizard')
