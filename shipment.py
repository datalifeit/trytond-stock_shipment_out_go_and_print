# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction
from trytond.wizard import (
    StateReport, StateTransition, StateView, Button, Wizard)
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import CompanyValueMixin
from trytond import backend

__all__ = ['ShipmentOut', 'ShipmentOutDoAndPrint', 'Configuration',
    'ConfigurationShipmentPrintState']


class Configuration:
    __name__ = 'stock.configuration'
    __metaclass__ = PoolMeta

    shipment_out_go_and_print_state = fields.MultiValue(
        fields.Selection([
            ('packed', 'Packed'),
            ('done', 'Done')], 'Shipment out print state',
            help='State to set shipment out when calling Go and print wizard.')
    )

    @classmethod
    def default_shipment_out_go_and_print_state(cls, **pattern):
        return cls.multivalue_model('shipment_out_go_and_print_state'
            ).default_shipment_out_go_and_print_state()


class ConfigurationShipmentPrintState(ModelSQL, CompanyValueMixin):
    "Stock Configuration Shipment Out Go and Print State"
    __name__ = 'stock.configuration.shipment_out_go_and_print_state'

    shipment_out_go_and_print_state = fields.Selection([
            ('packed', 'Packed'),
            ('done', 'Done')], 'Shipment out print state',
            help='State to set shipment out when calling Go and print wizard.')

    @staticmethod
    def default_shipment_out_go_and_print_state():
        return 'done'

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationShipmentPrintState, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('shipment_out_go_and_print_state')
        value_names.append('shipment_out_go_and_print_state')
        fields.append('company')
        migrate_property(
            'stock.configuration', field_names, cls, value_names,
            fields=fields)


class ShipmentOut:
    __name__ = 'stock.shipment.out'
    __metaclass__ = PoolMeta

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        cls._buttons.update({
            'go_and_print': {
                'icon': 'tryton-print',
                'invisible': Eval('state') != 'draft',
                'depends': ['state']
            },
        })

    @classmethod
    @ModelView.button_action(
        'stock_shipment_out_go_and_print.wizard_go_and_print')
    def go_and_print(cls, records):
        pass

    def _get_inventory_move(self, move):
        res = super(ShipmentOut, self)._get_inventory_move(move)
        try:
            res.lot = move.lot
        except AttributeError:
            pass
        return res

    @staticmethod
    def _get_move_io_key(move):
        key = (move.product.id,)
        try:
            key = key + (move.lot and move.lot.id or None,)
        except AttributeError:
            key = key + (None,)
        return key

    @classmethod
    def done(cls, records):
        Move = Pool().get('stock.move')
        for record in records:
            if len(record.inventory_moves) != len(record.outgoing_moves):
                continue
            for outgoing_move in record.outgoing_moves:
                if outgoing_move.unit_price == 0:
                    continue
                out_key = cls._get_move_io_key(outgoing_move)
                inventory_moves = [m for m in record.inventory_moves
                    if cls._get_move_io_key(m) == out_key and
                    m.unit_price == 0]
                if inventory_moves:
                    Move.write(inventory_moves, {
                        'unit_price': outgoing_move.unit_price})

        super(ShipmentOut, cls).done(records)


class ShipmentOutDoAndPrint(Wizard):
    """Stock shipment out do and print"""
    __name__ = 'stock.shipment.out.go_and_print'

    start = StateTransition()
    failed = StateView('stock.shipment.out.assign.failed',
                       'stock.shipment_out_assign_failed_view_form',
            [Button('Force Assign', 'force', 'tryton-go-next',
                    states={'invisible': ~Id(
                        'stock', 'group_stock_force_assignment').in_(
                        Eval('context', {}).get('groups', []))}),
             Button('Cancel', 'unassign', 'tryton-cancel', True)])
    force = StateTransition()
    unassign = StateTransition()
    print_ = StateReport('stock.shipment.out.delivery_note')

    def transition_start(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        Conf = pool.get('stock.configuration')

        conf = Conf(1)
        shipment = Shipment(Transaction().context['active_id'])
        Shipment.wait([shipment])
        if Shipment.assign_try([shipment]):
            Shipment.pack([shipment])
            if conf.shipment_out_go_and_print_state == 'done':
                Shipment.done([shipment])
            return 'print_'
        else:
            return 'failed'

    def transition_force(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        Conf = pool.get('stock.configuration')

        conf = Conf(1)
        shipment = Shipment(Transaction().context['active_id'])
        Shipment.assign_force([shipment])
        Shipment.pack([shipment])
        if conf.shipment_out_go_and_print_state == 'done':
            Shipment.done([shipment])
        return 'print_'

    def transition_unassign(self):
        Shipment = Pool().get('stock.shipment.out')

        shipment = Shipment(Transaction().context['active_id'])
        Shipment.draft([shipment])
        return 'end'

    def do_print_(self, action):
        data = {
            'id': Transaction().context['active_ids'].pop()
        }
        data['ids'] = [data['id']]
        return action, data

    def transition_print_(self):
        return 'end'
