datalife_stock_shipment_out_go_and_print
========================================

The stock_shipment_out_go_and_print module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_shipment_out_go_and_print/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_shipment_out_go_and_print)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
